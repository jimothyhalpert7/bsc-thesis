####
# RETURNS
####

specify_decimal <- function(x, k=4) as.numeric(unlist(format(round(x, k), nsmall=k)))

LuSentStrat <- function(eur_d, var_model, var_input, integration=1){
	#integration<-2
	
	var_fitted <- specify_decimal(var_model$varresult$EURUSD$fitted.values)
	#use_obsv <- (nrow(var_input)-(length(var_fitted) - integration)):nrow(eur_d)
	use_obsv <- (integration + 1):nrow(eur_d)
	eur_val_real <- eur_d[use_obsv - integration, 2]
	#eur_val_dates <- eur_d[head(use_obsv):(tail(use_obsv)[6] + (integration-1)), 1]
	eur_val_dates <- eur_d[use_obsv, 1]
	if (integration == 1) fit_eur_val <- exp(log(eur_val_real) + var_fitted)
	if (integration == 2) fit_eur_val <- exp(log(eur_val_real) + (diff(log(eur_val_real)) + var_fitted))
	#cbind(eur_val_real, fit_eur_val)
	#cbind(var_input[, 17], var_fitted)
	#rets_mat <- data.frame(A=numeric(0), B=numeric(0), C=numeric(0))
	rets_daily <- data.frame(A=numeric(0))
	for (i in 1:(length(fit_eur_val)-1)){
		if (eur_val_real[i] <= fit_eur_val[i+1]) ret_d <- ((eur_val_real[i+1]/eur_val_real[i]) - 1)
		if (eur_val_real[i] > fit_eur_val[i+1]) ret_d <- ((eur_val_real[i]/eur_val_real[i+1]) - 1)
		rets_daily[i, 1] <- ret_d
		#tmp <- c(eur_val_real[i], eur_val_real[i], fit_eur_val[i], ret_d)		
		#print(specify_decimal(tmp))
		#ret_cum <- cumprod(ret_mat[1:i, 1])
		#print(ret_cum)
		#ret_mat[i, 2] <- ret_cum
	}
	rets_daily <- specify_decimal(rets_daily)
	#rets_mat <- cbind(eur_d[(use_obsv-1), 1], eur_val_real, fit_eur_val, rets_d, cumprod(rets_d + 1), cumsum(rets_d))
	res_length <- length(fit_eur_val)
	rets_mat <- eur_val_real[1:res_length]
	rets_mat <- cbind(rets_mat, fit_eur_val[2:(res_length + 1)])
	rets_mat <- cbind(rets_mat, c(NA, rets_daily))
	rets_mat <- cbind(rets_mat, c(NA, cumsum(rets_daily )))	
	rets_mat <- cbind(rets_mat, c(NA, cumprod(rets_daily + 1)-1))	
	rets_mat[, 1:2] <- apply(rets_mat[, 1:2], 2, function(x) specify_decimal(x, 4)) 
	rets_mat[, 3:5] <- rets_mat[, 3:5]*100
	rets_mat[, 3:5] <- apply(rets_mat[, 3:5], 2, function(x) specify_decimal(x, 2)) 
	#rets_mat[2:length(rets_mat), 3] <- rets_daily#, cumprod(rets_daily + 1), cumsum(rets_daily))
	rets_mat <- cbind(NULL, rets_mat)
	rets_mat <- data.frame(eur_val_dates, rets_mat)
	colnames(rets_mat) <- c("Date", "Price.t", "Preiction.t+1", "Return", "Ret.cumsum", "Ret.cumprod")
	#apply(rets_mat, 1:2, specify_decimal)
	
	return(rets_mat)
}
#AkExportTable(LuSentStrat(eur_d, var_model, var_combcorr, 2), path_source, "strat_m2.xlsx")
LuSentStrat(eur_d, var_model, var_combcorr, 2)

