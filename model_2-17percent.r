eur_d <- AkDateHead(eur_df)[62:(61+nrow(sent_cast_d)), 1:2]

var_input <- diff(AkGetVarMat(sent_cast_d, eur_d))
adf_results <- apply(var_input, 2, function(x) adf.test(x)$p.value)
adf_results
adf_l1 <- adf_results[adf_results <= 0.05]
adf_l2 <- adf_results[adf_results > 0.05]
var_mat_diff <- diff(var_input[, names(adf_l2)])
#var_input <- cbind(var_input[1:length(var_mat_diff), names(adf_l1)], var_mat_diff)
#colnames(var_input)[18] <- "EURUSD"
apply(var_input, 2, function(x) PP.test(x)$p.value)


var_nocorr <- AkRemoveCor(var_input, "EURUSD", 0.3)
var_combcorr <- AkCombineCor(var_nocorr, corr_high=0.3)
VARselect(var_combcorr, lag.max=12, type="none")
var_model <- VAR(var_combcorr, p=1, type="none")
summary(var_model)$varresult$EURUSD

# VAR TESTS
plot(var_model)
roots(var_model, modulus=TRUE) # Eigenvvalues, < 1 means stationarity
arch.test(var_model, multivariate="FALSE")
#plot(stability(var_model))
plot(stability(var_model)$stability$EURUSD)
plot(stability(var_model, type="OLS-MOSUM")$stability$EURUSD)

x_names <- unlist(dimnames(var_model$y))
x_names <- x_names[x_names != "EURUSD"]
causality(var_model, cause=x_names)
