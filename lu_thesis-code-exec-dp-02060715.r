#AkInstallPackages()
#update.packages(ask = FALSE)
AkLoadLibraries()

describe <- psych:::describe
tm_tag_score <- tm_term_score
#path_source <- "C:/Users/User/Google disks/lu-thesis/source/"
#path_source <- "C:/Users/ECENTA AG/Google Drive/lu-thesis/source/"
path_source <- "C:/Users/ajkos/Google Drive/source/"
path_output <- "C:/Users/ajkos/Google Drive/source/toGit/output"

news_df_nov <- AkLoadNews("input-11.csv")
news_df_jan <- AkLoadNews("input-0101.csv")

news_dfs <- list(news_df_nov, news_df_jan)


eur_df_nov <- AkLoadPriceData("EURUSD_Candlestick_1_h_BID.csv")
eur_df_jan <- AkLoadPriceData("EURUSD_0111-1705.csv")


news_df <- news_df_nov[, 1:6]

#########

#news_df <- news_df_jan[, 1:6]
news_lengths <- news_df[, 5:6]
#no filter
#news_lengths <- as.data.frame(news_df_nov, news_df_jan)[, 5:6]
AkDescribe(news_lengths)
#Title length > 6
news_lengths <- news_lengths[news_lengths[, "Title length"] >= 6, ]
AkDescribe(news_lengths)
#Title length > 6 & Body length > 12
news_lengths <- news_lengths[news_lengths[, "Body length"] >= 12, ]
AkDescribe(news_lengths)

news_lengths_titles <- news_df[, c(1,5)]
news_lengths_titles[3] <- "titles"
news_lengths_bodies <- news_df[, c(1,6)]
news_lengths_bodies[3] <- "bodies"
#news_lengths_narros


######## PLOT LENGTHS

news_dfs <- lapply(news_dfs, function(x) x[, 1:6])
news_dfs <- lapply(news_dfs, function(news_df) news_df[(news_df[, "Title length"] > 6) & (news_df[, "Body length"] > 12), ])

news_lengths <- news_df[, 5:6]
AkExportTable(AkDescribe(news_lengths), path_output, "desc_lengths_m1.xlsx")

news_lengths_wide <- news_df[, c(1, 5, 6)]
news_lengths_narrow <- melt(news_lengths_wide, id=c("time"), variable.name="group", value.name = "value")
news_lengths_h <- AkIndexExt(news_lengths_narrow, 1)
AkAnnotatedTimeLine(news_lengths_h, "allfixed")

#########


#print(xtable(describe(eur_df)[2, c(2, 3, 4, 5, 8, 9)]))
#xtable(AkDescribe(eur_df)[2, ])
AkDescribe(eur_df_nov)[2, ]
AkDescribe(eur_df_jan)[2, ]

# Filter the dataframe, by min length of title an body
news_df <- news_df[(news_df[, "Title length"] > 6) & (news_df[, "Body length"] > 12), 1:4]
unique(news_df[, "source"])  # Unique sources
length(unique(news_df[, "source"]))  # number of unique sources

as.data.frame(unique(news_dfs[[1]]$source))
length(unique(news_df[, "source"]))
##################

# Descriptive statistics

##################

#eur_df_nov <- AkLoadPriceData("EURUSD_Candlestick_1_h_BID.csv")
#eur_df_jan <- AkLoadPriceData("EURUSD_0111-1705.csv")
#AkExportTable(AkDescribe(eur_df_nov)[2:4, ], path_output, "desc_prices_m1.xlsx")

# Word_cloud
AkSentStats(news_df, word_cloud=TRUE)

eur_df <- AkLoadPriceData("EURUSD_0111-1705.csv")
AkDescribe(eur_df)[2:4, ]

eur_df_open <- AkLoadPrice(eur_df)
AkAnnotatedTimeLine(eur_df_open, "allmaximized")

#print(xtable(describe(eur_df)[2, c(2, 3, 4, 5, 8, 9)]))
#xtable(AkDescribe(eur_df)[2, ])

###################

AkSentDescribe <- function(sent_wide){	
	result <- cbind(AkDescribe(sent_wide)[3:7, ], t(apply(sent_wide[,3:7], 2, function(x) quantile(x, na.rm=TRUE))))
	return(result)
	#print(xtable(AkDescribe(sent_wide)))
	#print(t(apply(sent_wide[,3:7], 2, function(x) quantile(x, na.rm=TRUE))))
	#sent_wide[, 4][sent_wide[, 4] >= 1]
}
##sent_wides <- lapply(news_dfs, AkSentStats)
##lapply(sent_wides, AkSentDescribe)
##AkExportTable(lapply(sent_wides, AkSentDescribe), path_output, "desc_sent_wides.xlsx")

##################

##sent_wide <- lapply(news_dfs, AkSentStats)
sent_wide <- t(sent_wide)
#sent_wide <- sent_wide_1
sent_narrow <- melt(sent_wide, id=c("time", "source"), variable.name="group", value.name = "value")
sent_narrow_d <- AkIndexExt(sent_narrow, 100)
# Graph 
AkAnnotatedTimeLine(sent_narrow_d, "allfixed")	


#sent_melt_d <- AkIndexExt(sent_melt_df, 100)

#Sentiment index as 3rd argument in c

sent_pos_df <- sent_wide[, c(1, 2, 5)]
colnames(sent_pos_df) <- c("time", "source", "value")
#sent_narrow_h <- AkCreateIndex(sent_pos_df, 1, "hourly")
#TODO
#sent_narrow_h <- sent_narrow_h[4:nrow(sent_narrow_h),]
sent_narrow_d <- AkCreateIndex(sent_pos_df, 100, "daily")

#sent_cast_h <- AkCast(sent_pos_df, 0.5, daily=FALSE)
sent_cast_d <- AkCast(sent_pos_df, 0, daily=TRUE)

eur_df <- AkLoadPrice(eur_df)
eur_d <- AkDateHead(eur_df)[1:nrow(sent_cast_d), 1:2]


var_input <- AkGetVarMat(sent_cast_d, eur_d)
adf_results <- apply(var_input, 2, function(x) adf.test(x)$p.value)
adf_results

adf_l1 <- adf_results[adf_results <= 0.05]
adf_l1
adf_l2 <- adf_results[adf_results > 0.05]
adf_l2
adf_l2 <- apply(var_input[, names(adf_l2)], 2, function(x) adf.test(diff(x))$p.value)
adf_l2

#summary(ur.df(diff(var_mat[, 1]), type = "none", lags = 1))
var_mat_diff <- diff(var_input[, names(adf_l2)])
#var model without the unit root in variables
var_input <- cbind(var_input[1:nrow(var_mat_diff), names(adf_l1)], var_mat_diff)
#var_input <- cbind(var_input[1:length(var_mat_diff), names(adf_l1)], var_mat_diff)

#x <- rnorm(1000)
#PP.test(rnorm(1000))
#y <- cumsum(rnorm(1000)) # has unit root > 0.05
#PP.test(y)
apply(var_input, 2, function(x) PP.test(x)$p.value)


var_nocorr <- AkRemoveCor(var_input, "EURUSD", 0.5)
#embed(var_nocorr)

#rcorr(AkCombineCor(var_nocorr, corr_high=0.5))
#apply(AkCombineCor(var_nocorr, corr_high=0.5), 2, function(x) adf.test(x)$p.value)
var_combcorr <- AkCombineCor(var_nocorr, corr_high=0.5)
#acf(var_combcorr)$acf

#Information Criteria for optimal lag selection
#VARselect(var_input, lag.max=4, type="none")
#VARselect(AkCombineCor(var_nocorr, corr_high=0.1), lag.max=4, type="none")
VARselect(AkCombineCor(var_nocorr, corr_high=0.1), lag.max=4, type="none")

var_model <- VAR(var_combcorr, p=1, type="none")
summary(var_model)$varresult$EURUSD

#summary(VAR(AkCombineCor(var_nocorr, corr_high=0.5), ic="AIC", lag.max=4, type="none"))$varresult$EURUSD
#causality(VAR(AkCombineCor(var_nocorr, corr_high=0.5), ic="AIC", lag.max=4, type="none", type="none"))


# VAR TESTS
plot(var_model)
roots(var_model, modulus=TRUE) # Eigenvvalues, < 1 means stationarity
arch.test(var_model, multivariate="FALSE")
#plot(stability(var_model))
plot(stability(var_model)$stability$EURUSD)


x_names <- unlist(dimnames(var_model$y))
x_names <- x_names[x_names != "EURUSD"]
causality(var_model, cause=x_names)

#lapply(x_names, function (x) LuVarsTest(var_input, x))

