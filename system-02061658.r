#R CMD INSTALL --build --compile-both tm.tar.gz
# _h means 'hourly', _d means 'daily'

AkInstallPackages <- function()
{
	#install.packages("slam")
	install.packages("rJava")
	install.packages("Defaults")
	install.packages("zoo")
	install.packages("tm")
	#install.packages("D:\\lu-thesis\\tm.tar.gz", repos = NULL, type="source")
	#install.packages("D:\\dwnlds\\brwsr\\tm_0.5-10-mod\\tm.zip", repos = NULL, type="source")
	install.packages("tm.plugin.webmining")
	install.packages("tm.plugin.tags", repos = "http://datacube.wu.ac.at", type = "source")
	#install.packages("tm.plugin.sentiment", repos="http://R-Forge.R-project.org", dependencies=TRUE)
	install.packages("devtools")
	install_github("mannau/tm.plugin.sentiment")
	install.packages("ggplot2")
	install.packages("googleVis")
	install.packages("reshape2")
	install.packages("quantmod")
	install.packages("TFX")
	install.packages("fImport")
	install.packages("Rmisc")
	install.packages("pracma")
	install.packages("vars")
	install.packages("tseries")
	install.packages("nlts")
	#install.packages("aod")
	#Wald test
	install.packages("survey")
	#irf function plot
	install.packages("irtoys")
	#copypaste the install directory link
	install.packages("Rstem") #, repos = "http://www.omegahat.org/R")
	download.file("http://cran.r-project.org/src/contrib/Archive/sentiment/sentiment_0.2.tar.gz", "sentiment.tar.gz")
	install.packages("sentiment.tar.gz", repos=NULL, type="source")
	install.packages("psych")
	install.packages("xtable")
	#install.packages("stringi")
	install.packages("wordcloud")
	install.packages("leaps")
	install.packages("survival")
	install.packages("Hmisc")
	#install.packages("xlsReadWrite", repos = "http://cran.r-project.org/src/contrib/Archive/xlsReadWrite/xlsReadWrite_1.5.4.tar.gz", type = "source")
	#install.packages("xlsReadWrite")
	install.packages("xlsx")
}
#AkInstallPackages()

AkLoadLibraries <- function()
{
	#http://stackoverflow.com/questions/7019912/using-the-rjava-package-on-win7-64-bit-with-r
	#if (Sys.getenv("JAVA_HOME")!="")
	  #Sys.setenv(JAVA_HOME="")
	library(rJava)
	library(tm)
	library(tm.plugin.tags)
	library(tm.plugin.webmining)
	library(devtools)
	#library(tm.plugin.sentiment)
	library(ggplot2)
	library(googleVis)
	library(reshape2)
	library(quantmod)
	library(TFX)
	library(fImport)
	library(Rmisc)
	library(pracma)
	library(vars)
	library(tseries)
	library(nlts)
	#library(aod)
	library(survey)
	library(irtoys)
	library(Rstem)
	library(sentiment)
	library(psych)
	library(xtable)
	#library(stringi)
	library(wordcloud)
	library(leaps)
	library(survival)
	library(Hmisc)
	#library(xlsReadWrite)
	library(xlsx)
}

AkExportTable <- function(table_df, path_source, table_name){
	write.xlsx(table_df, paste(path_source, table_name, sep=""))
}

#Export data as googleVis html graph
AkAnnotatedTimeLine <- function(input_df, scale_type){
	plot(gvisAnnotatedTimeLine(
		input_df,
		datevar="time",
		numvar="value",
		idvar="group",
		options=list(
			#displayAnnotations=TRUE,
			#legendPosition=newRow,
			width=1200,
			height=600,
		#scaleColumns="[0,2]",
		scaleType=scale_type,
		gvis.editor="Edit",
		displayExactValues="true",
		thickness="1"	
		)	
		)
	)
}

AkDescribe <- function(x){
	return(as.data.frame(psych:::describe(x))[, c(2, 3, 4, 5, 7, 8, 9)])
}

AkLoadNews <- function(file_name){
	#file_name <- "input-11.csv"
	news_csv <- read.csv(paste(path_source, file_name, sep=""), sep = "|", row.names=NULL, header = FALSE)
	news_dataframe <- as.data.frame(news_csv)
	news_dataframe[, 5] <- data.frame(nchar(as.vector(news_dataframe[, 3])))
	news_dataframe[, 6] <- data.frame(nchar(as.vector(news_dataframe[, 4])))
	colnames(news_dataframe) <- c("time", "source", "Title", "Body", "Title length", "Body length")	
	return(news_dataframe)
}


# Create document-term matrix	
GetDtm <- function(corpus, control, sparsity = 0.999, tdm=0)
{
	#corpus <- corp.list[[1]]
	dtm <- DocumentTermMatrix(corpus, control)
	if (tdm == 1) dtm <- TermDocumentMatrix(corpus, control)
	dtm  <- removeSparseTerms(dtm, sparsity)
	#This function call removes those terms which have at least a 40 percentage
	#of sparse (i.e., terms occurring 0 times in a document) elements
	dtm <-inspect(dtm)
	#rownames(dtm) <- corp.list[[2]]
	dtm.df <- as.data.frame(dtm, stringAsFactors = FALSE)
	return(dtm.df)
}

GraphWordcloud <- function(dtm.df){
	dtm.mat <- as.matrix(dtm.df)
	word_freqs = sort(colSums(dtm.mat), decreasing=TRUE)
	dm = data.frame(word=names(word_freqs), freq=word_freqs)
	wordcloud(dm$word, dm$freq, random.order=FALSE, colors=brewer.pal(8, "Dark2"))
}

AkDateConvert <- function(input_date){
	tst.date <- input_date
	tst.date.string <- paste(trunc(tst.date/10000), trunc((tst.date%%10000)/100), "2013", (tst.date%%100), "00", sep=" ")
	tst.date <- strptime(tst.date.string, "%d %m  %Y %H %M")
	#tst.date <- as.Date(tst.date)
	return(tst.date)
}

as.numeric.factor <- function (x) {
	return(as.numeric(levels(x))[x])
}

#1 for hourly index, 100 for daily
AkIndexExt <- function (sent_df, daily){
	#sent_df <- sent_narrow
	#daily <- 100
	sdg_h <- data.frame(sent_df$time, sent_df$group, sent_df$value)
	colnames(sdg_h) <- c("time", "group", "value")
	sdg_h[, 1] <- trunc(as.numeric.factor(as.factor(sent_df[, 1]))/(100*daily))*daily

	colnames(sdg_h) <- c("time", "group", "value")
	sdg_h <- aggregate(sdg_h, by=list(sdg_h$group, sdg_h$time), mean, na.rm=TRUE)
	sdg_h <- sdg_h[, c(2, 5, 1)] 
	
	sdg_h[1] <- lapply(sdg_h[1], function (x) AkDateConvert(x))
	#ak.AnnotatedTimeLine(sdg_h_formatted[1:2000,])
	#sent_index_all_h <- data.frame(sdg_h_formatted[[1]],sdg_h_formatted[[3]])
	#sent_index_all_h <- aggregate(sent_index_all_h[[2]], list(sent_index_all_h[[1]]), mean)
	#sdg_h_formatted[3] <- group_name
	colnames(sdg_h) <- c("time", "value", "group")
	sdg_h <- sdg_h[order(sdg_h$time),]
	rownames(sdg_h) <- NULL
	return(sdg_h)
}

#1 for hourly index, 100 for daily
#sent_df[, c("time", "source", "value")]
AkCreateIndex <- function (sent_df, daily, group_name){
	#sent_df <- sent_pos_df
	#daily <- 100
	sdg_h <- data.frame(sent_df[1], sent_df[3])
	sdg_h[,1] <- trunc(as.numeric.factor(as.factor(sent_df[,1]))/(100*daily))*daily
	sdg_h <- aggregate(sdg_h[2], sdg_h[1], mean, na.rm=TRUE)
	sdg_h_formatted <- sdg_h
	sdg_h_formatted[1] <-lapply(sdg_h[1], function (x) AkDateConvert(x))
	sdg_h_formatted[3] <- group_name
	colnames(sdg_h_formatted) <- c("time", "value", "group")
	sdg_h_formatted <- sdg_h_formatted[order(sdg_h_formatted$time),]
	rownames(sdg_h_formatted) <- NULL
	return(sdg_h_formatted)
}

AkLoadPrice <- function(eur_df, row_id=5){
	eur_graph <- data.frame(strptime(eur_df[,1], "%d.%m.%Y %H:%M:%S"), eur_df[row_id])
	eur_graph[3] <- "EURUSD"
	#eur_graph[,1] <- lapply(eur_graph[,1], function (x) as.POSIXct((x)[[1]]))
	colnames(eur_graph) <- c("time", "value", "group")
	return(eur_graph)
}

AkLoadPriceData <- function(file_name){
	eur.csv <- read.csv(paste(path_source, file_name, sep=""), sep = ",", header = TRUE)
	eur.df <-as.data.frame(eur.csv)	
	return(eur.df)
}	

AkDateConvert2 <- function(input_date){
	tst.date <- input_date
	tst.date.string <- paste(trunc(tst.date/1000000), trunc((tst.date%%1000000)/10000), "2013", trunc((tst.date%%10000)/100),  (tst.date%%100), sep=" ")
	tst.date <- strptime(tst.date.string, "%d %m  %Y %H %M")
	return(tst.date)
}

#Get sentiment table with sources as colnames
AkCast <- function(sent_df, na_max, daily){
	#sent_df <- sent_pos_df
	#na_max <- 0
	daily <- TRUE
	
	sent_df$time <- as.numeric.factor(as.factor(sent_df$time))
	sent_df$time <- unlist(lapply(sent_df$time, function (x) trunc(x/100)*100))
	
	if (daily==TRUE){
		sent_df[, 1] <- t(as.data.frame(lapply(sent_df[, 1], function (x) as.Date(AkDateConvert2(x)))))
		sent_df <- aggregate(sent_df, by=as.list(sent_df[, 1:2]), mean, na.rm=TRUE)[, c(1, 2, 5)]
	}else{
		# TODO
		# set date for hours 
		#sent_cast <- aggregate(sent_cast, by=list(sent_cast$time), mean, na.rm=TRUE)
	}
	#sent_df <- aggregate(sent_df, by=as.list(sent_df[, 1:2]), mean, na.rm=TRUE)[, c(1, 2, 5)]
	sent_df <- dcast(sent_df, time ~ group, value.var="value", mean)
	
	#NA less than nrow * na_max
	sent_df <- sent_df[,colSums(is.na(sent_df)) <= (nrow(sent_df)*na_max)]
	#colnames(sent_cast)[1] <- "time"
	return(sent_df)
}

AkDateHead <- function(input_df, fun="head"){		
	#input_df <- eur_df
	input_df <- eur_df
	result <- input_df
	result$time <- as.Date(input_df$time)
	#take head of each date group == first value of the day
	result <- aggregate(.~time, result[, 1:2], FUN=fun, 1)
	result[, 3] <- input_df[1, 3]
	return(result)
}

AkGetVarMat <- function(sent_cast, eur_d){
	#cannot take logs of neg numbers -> only take pos sentiment
	#var_mat <- cbind((sent_all_h[, 2] + 1), eur_graph[, 2])
	#sent_delt <- apply(as.matrix(sent_cast), 2, function(x) (as.ts(x)/lag(as.ts(x), 1) - 1))
	sent_delt <- apply(as.matrix(sent_cast[, -1]), 2, function(x) as.ts(x) - lag(as.ts(x), 1))
	#(sent_cast[, -1]/lag(sent_cast[, -1], k=1) - 1)
	#eur_delt_log <- diff(log(as.numeric.factor(eur_d[, -1])))
	eur_delt_log <- diff(log(eur_d[, -1]))
	var_mat <- cbind(sent_delt, eur_delt_log)
	var_mat <- as.matrix(var_mat)	
	colnames(var_mat) <- c(colnames(sent_cast[, -1]), "EURUSD") 
	return(var_mat)
}

# Remove variables whose correlation with EURUSD
# has a p-value <= 0.05
AkRemoveCor <- function(var_mat, price_name, corr_high){
	#var_mat <- var_input
	#price_name <- "EURUSD"
	corr_eur <- rcorr(var_mat)$P[ ,price_name]
	print(rcorr(var_mat)$r[ ,price_name])
	print(corr_eur)
	input_mat <- var_mat
		corr_p <- rcorr(input_mat)$P[ ,price_name]
		corr_r <- rcorr(input_mat)$r[ ,price_name]
		corr_p_names <- names(corr_p[(corr_p <= 0.05) & !is.na(corr_p)])
		corr_r_names <- names(corr_r[(abs(corr_r) >= corr_high) & (abs(corr_r) != 1.00)])
		corr_names <- c(corr_p_names, corr_r_names)
		corr_names <- corr_names[duplicated(corr_names)]
	var_mat <- input_mat
	if (length(corr_names) > 0) var_mat <- var_mat[, -which(colnames(var_mat) %in% corr_names)]
	return(var_mat)
}

# Make averages from highly correlated series.
# Highly correlated defined by p-value <= 0.05 and
# correlation of 'corr_high', or higher
AkCombineCor <- function(input_mat, corr_high){
	n <- ncol(input_mat)
	for (i in 1:n){
		if (i > ncol(input_mat)) return(input_mat)
		print(i)
		print(colnames(input_mat)[i])
		#print(ncol(rcorr(input_mat)$P))
		corr_p <- rcorr(input_mat)$P[ ,i]
		corr_r <- rcorr(input_mat)$r[ ,i]
		corr_p_names <- names(corr_p[(corr_p <= 0.05) & !is.na(corr_p)])
		corr_r_names <- names(corr_r[(abs(corr_r) >= corr_high) & (abs(corr_r) != 1.00)])
		corr_names <- c(corr_p_names, corr_r_names)
		corr_names <- corr_names[duplicated(corr_names)]
		if (length(corr_names) > 0) {
			print(corr_names)
			print(corr_p)
			print(corr_r)
			indx <- apply(cbind(input_mat[, i], input_mat[, corr_names]), 1, mean)
			#colnames(indx) <- 'indx'
			#print(indx)
			index_name <- paste0("indx", colnames(input_mat)[i])
			input_mat <- cbind(input_mat[, -which(colnames(input_mat) %in% c(colnames(input_mat)[i], corr_names))], indx)
			colnames(input_mat)[ncol(input_mat)] <- index_name
			#n <- ncol(input_mat)
			#print(ncol(input_mat))
			#input_mat <- input_mat
		}
	}
	return(input_mat)
}

# Make averages from highly correlated series.
# Highly correlated defined by p-value <= 0.05 and
# correlation of 'corr_high', or higher
AkCombineCorAlt <- function(input_mat, corr_high){
	n <- ncol(input_mat)
	i <- 1
	
	corr_p <- rcorr(input_mat)$P[ ,i]
	corr_r <- rcorr(input_mat)$r[ ,i]
	corr_p_names <- names(corr_p[(corr_p <= 0.05) & !is.na(corr_p)])
	corr_r_names <- names(corr_r[(abs(corr_r) >= corr_high) & (abs(corr_r) != 1.00)])
	corr_names <- c(corr_p_names, corr_r_names)
	corr_names <- corr_names[duplicated(corr_names)]

	while (i <= n){
		if (i > ncol(input_mat)) return(input_mat)
		print(i)
		print(colnames(input_mat)[i])
		#print(ncol(rcorr(input_mat)$P))
		if (length(corr_names) > 0) {
			print(corr_names)
			print(corr_p)
			print(corr_r)
			indx <- apply(cbind(input_mat[, i], input_mat[, corr_names]), 1, mean)
			#colnames(indx) <- 'indx'
			#print(indx)
			index_name <- paste0("indx", colnames(input_mat)[i])
			input_mat <- cbind(indx, input_mat[, -which(colnames(input_mat) %in% c(colnames(input_mat)[i], corr_names))])
			colnames(input_mat)[1] <- index_name
			#colnames(input_mat)[ncol(input_mat)] <- index_name
			#n <- ncol(input_mat)
			#print(ncol(input_mat))
			#input_mat <- input_mat
		}
		corr_p <- rcorr(input_mat)$P[ ,i]
		corr_r <- rcorr(input_mat)$r[ ,i]
		corr_p_names <- names(corr_p[(corr_p <= 0.05) & !is.na(corr_p)])
		corr_r_names <- names(corr_r[(abs(corr_r) >= corr_high) & (abs(corr_r) != 1.00)])
		corr_names <- c(corr_p_names, corr_r_names)
		corr_names <- corr_names[duplicated(corr_names)]
		if (length(corr_names) <= 0) i <- i + 1
		n <- ncol(input_mat)		
	}
	return(input_mat)
}
#Returns wide sent_df: time, source, polarity, subjectivity, ...
#AkLoadSent(news_df, news_corpus, tdmParams)
AkLoadSent <- function(news_df, news_corpus, tdmParams){
	#emotions_df <- as.data.frame(lapply(nt.df[1, 3], function (x) classify_emotion(x, algorithm="bayes", prior=1.0))[[1]][1:6])
	#colnames(emotions_df) <- colnames(classify_emotion("get colnames", algorithm="bayes", prior=1.0))
	#polarity_df <- as.data.frame(lapply(nt.df[1, 3], function(x) classify_polarity(x, algorithm="bayes"))[[1]][3])
	#colnames(polarity_df) <- colnames(
	#grading sentiment of texts only, to increase productivity
	text.corpus <- score(news_corpus, tdmParams)
	#meta(text.corpus)
	sent_df <- data.frame(news_df[[1]], news_df[[2]], meta(text.corpus)[, -1])
	colnames(sent_df) <- c("time", "source", colnames(sent_df)[3:ncol(sent_df)])
	#TODO: temp
	#bob <- data.frame(lapply(bob, as.character), stringsAsFactors=FALSE)
	return(sent_df)
}

AkSentStats <- function(news_df, word_cloud=FALSE){
	# Make a stopwords list
	stopwords <- unique(c(stopwords("SMART"), stopwords("en")), "for")

	# Controls for Term-Document matrix creation
	tdmParams <- list(tolower = TRUE,
							removePunctuation = TRUE,
							removeNumbers = TRUE,
							removeWords = list(stopwords),
							stripWhitespace = TRUE,
							stemDocument = TRUE,
							minWordLength = 3,
							weighting = weightTf
						)

	news_corpus <- Corpus(VectorSource(news_df[[3]]))

	if (word_cloud == TRUE){
		dtm <- GetDtm(news_corpus, tdmParams)
		GraphWordcloud(dtm)
	}else{
		sent_wide <- AkLoadSent(news_df, news_corpus, tdmParams)
		return(sent_wide)
	}
}
#AkSentStats(news_df, word_cloud=TRUE)
####
# RETURNS
####

specify_decimal <- function(x, k=4) as.numeric(unlist(format(round(x, k), nsmall=k)))

LuSentStrat <- function(eur_d, var_model, var_input, integration=1){
	#integration<-2
	
	var_fitted <- specify_decimal(var_model$varresult$EURUSD$fitted.values)
	#use_obsv <- (nrow(var_input)-(length(var_fitted) - integration)):nrow(eur_d)
	use_obsv <- (integration + 1):nrow(eur_d)
	eur_val_real <- eur_d[use_obsv - integration, 2]
	#eur_val_dates <- eur_d[head(use_obsv):(tail(use_obsv)[6] + (integration-1)), 1]
	eur_val_dates <- eur_d[use_obsv, 1]
	if (integration == 1) fit_eur_val <- exp(log(eur_val_real) + var_fitted)
	if (integration == 2) fit_eur_val <- exp(log(eur_val_real) + (diff(log(eur_val_real)) + var_fitted))
	#cbind(eur_val_real, fit_eur_val)
	#cbind(var_input[, 17], var_fitted)
	#rets_mat <- data.frame(A=numeric(0), B=numeric(0), C=numeric(0))
	rets_daily <- data.frame(A=numeric(0))
	for (i in 1:(length(fit_eur_val)-1)){
		if (eur_val_real[i] <= fit_eur_val[i+1]) ret_d <- ((eur_val_real[i+1]/eur_val_real[i]) - 1)
		if (eur_val_real[i] > fit_eur_val[i+1]) ret_d <- ((eur_val_real[i]/eur_val_real[i+1]) - 1)
		rets_daily[i, 1] <- ret_d
		#tmp <- c(eur_val_real[i], eur_val_real[i], fit_eur_val[i], ret_d)		
		#print(specify_decimal(tmp))
		#ret_cum <- cumprod(ret_mat[1:i, 1])
		#print(ret_cum)
		#ret_mat[i, 2] <- ret_cum
	}
	rets_daily <- specify_decimal(rets_daily)
	#rets_mat <- cbind(eur_d[(use_obsv-1), 1], eur_val_real, fit_eur_val, rets_d, cumprod(rets_d + 1), cumsum(rets_d))
	res_length <- length(fit_eur_val)
	rets_mat <- eur_val_real[1:res_length]
	rets_mat <- cbind(rets_mat, fit_eur_val[2:(res_length + 1)])
	rets_mat <- cbind(rets_mat, c(NA, rets_daily))
	rets_mat <- cbind(rets_mat, c(NA, cumsum(rets_daily )))	
	rets_mat <- cbind(rets_mat, c(NA, cumprod(rets_daily + 1)-1))	
	rets_mat[, 1:2] <- apply(rets_mat[, 1:2], 2, function(x) specify_decimal(x, 4)) 
	rets_mat[, 3:5] <- rets_mat[, 3:5]*100
	rets_mat[, 3:5] <- apply(rets_mat[, 3:5], 2, function(x) specify_decimal(x, 2)) 
	#rets_mat[2:length(rets_mat), 3] <- rets_daily#, cumprod(rets_daily + 1), cumsum(rets_daily))
	rets_mat <- cbind(NULL, rets_mat)
	rets_mat <- data.frame(eur_val_dates, rets_mat)
	colnames(rets_mat) <- c("Date", "Price.t", "Preiction.t+1", "Return", "Ret.cumsum", "Ret.cumprod")
	#apply(rets_mat, 1:2, specify_decimal)
	
	return(rets_mat)
}