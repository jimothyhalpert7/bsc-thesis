AkLoadLibraries()
describe <- psych:::describe
tm_tag_score <- tm_term_score
path_source <- "C:/Users/User/Google disks/lu-thesis/source/"
#path_source <- "C:/Users/ECENTA AG/Google Drive/lu-thesis/source/"
news_df <- AkLoadNews("input-11.csv")
#news_df <- AkLoadNews("input-0101.csv")
eur_df <- AkLoadPriceData("EURUSD_Candlestick_1_h_BID.csv")
#eur_df_all <- AkLoadPriceData("EURUSD_0111-1705.csv")

news_df <- news_df[, 1:6]
# Filter the dataframe, by min length of title an body
news_df <- news_df[(news_df[, "Title length"] > 6) & (news_df[, "Body length"] > 12), 1:4]

# Make a stopwords list
stopwords <- unique(c(stopwords("SMART"), stopwords("en")), "for")
# Controls for Term-Document matrix creation
news_control <- list(tolower = TRUE,
						removePunctuation = TRUE,
						removeNumbers = TRUE,
						removeWords = list(stopwords),
						stripWhitespace = TRUE,
						stemDocument = TRUE,
						minWordLength = 3,
						weighting = weightTf
					)
news_corpus <- Corpus(VectorSource(news_df[[3]]))
sent_wide <- AkLoadSent(news_df, news_corpus, news_control)
#sent_narrow <- melt(sent_wide, id=c("time", "source"), variable.name="group", value.name = "value")
#sent_narrow_d <- AkIndexExt(sent_narrow, 100)

### PRE-VAR

sent_pos_df <- sent_wide[, c(1, 2, 5)]
colnames(sent_pos_df) <- c("time", "group", "value")
#AkAnnotatedTimeLine(sent_narrow_d, "allfixed")
#sent_narrow_d <- AkCreateIndex(sent_pos_df, 100, "daily")
sent_cast_d <- AkCast(sent_pos_df, 0, daily=TRUE)
as.data.frame(colnames(sent_cast_d)[-1])

eur_df <- AkLoadPrice(eur_df)
eur_d <- AkDateHead(eur_df)[1:nrow(sent_cast_d), 1:2]

### VAR

var_input <- AkGetVarMat(sent_cast_d, eur_d)
adf_results <- apply(var_input, 2, function(x) adf.test(x)$p.value)
adf_l1 <- adf_results[adf_results <= 0.05]
adf_l2 <- adf_results[adf_results > 0.05]
#adf_l2 <- apply(var_input[, names(adf_l2)], 2, function(x) adf.test(diff(x))$p.value)
var_mat_diff <- diff(var_input[, names(adf_l2)])
var_input <- cbind(var_input[1:nrow(var_mat_diff), names(adf_l1)], var_mat_diff)
#1st table
#AkExportTable(as.data.frame(apply(var_input, 2, function(x) adf.test(x)$p.value)), path_source, "5221.xlsx")

var_nocorr <- AkRemoveCor(var_input, "EURUSD", 0.5)
#for table
eur_corr <- cbind(rcorr(var_input)$r[, "EURUSD"], rcorr(var_input)$P[, "EURUSD"])
colnames(eur_corr) <- c("r", "P")
#AkExportTable(eur_corr, path_source, "5222.xlsx")

var_combcorr <- AkCombineCor(var_nocorr, corr_high=0.5)

var_model <- VAR(var_combcorr, p=1, type="none")

### VAR ANALYSIS

summary(var_model)$varresult$EURUSD
plot(var_model)
roots(var_model, modulus=TRUE) # Eigenvvalues, < 1 means stationarity
arch.test(var_model, multivariate="FALSE")
plot(stability(var_model)$stability$EURUSD)
x_names <- unlist(dimnames(var_model$y))
x_names <- x_names[x_names != "EURUSD"]
causality(var_model, cause=x_names)

